import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogTitle from "@mui/material/DialogTitle";
import TextField from "@mui/material/TextField";
import React, { useState, useEffect } from "react";
import "../assets/css/Homepage.css";

export const Homepage = () => {
  const [open, setOpen] = useState(false);
  const [name, setName] = useState("");
  const [address, setAddress] = useState("");
  const [hobby, setHobby] = useState("");
  const [userAdded, setUserAdded] = useState(false);
  const [userList, setUserList] = useState([]);
  const [showScrollButton, setShowScrollButton] = useState(false);

  const handleOpen = () => {
    setOpen(true);
  };

  const handleCancel = () => {
    setOpen(false);
    setName("");
    setAddress("");
    setHobby("");
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleSave = () => {
    const newUser = { name, address, hobby };
    setUserList([...userList, newUser]);
    setOpen(false);
    setUserAdded(true);
    setName("");
    setAddress("");
    setHobby("");
  };

  const handleNameChange = (e) => {
    setName(e.target.value);
  };

  const handleAddressChange = (e) => {
    setAddress(e.target.value);
  };

  const handleHobbyChange = (e) => {
    setHobby(e.target.value);
  };

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop =
        document.documentElement.scrollTop || document.body.scrollTop;
      setShowScrollButton(scrollTop > 0);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  return (
    <div>
      <div className="headerContent">
        <div className="titleApp">
          <span className="titleName">My App</span>
        </div>
        <div className="addUser">
          <Button variant="contained" onClick={handleOpen}>
            Add User
          </Button>
          <Dialog open={open} onClose={handleClose}>
            <DialogTitle>Add User</DialogTitle>
            <DialogContent>
              <TextField
                autoFocus
                margin="dense"
                id="name"
                label="Name"
                type="text"
                fullWidth
                variant="filled"
                value={name}
                onChange={handleNameChange}
              />
              <TextField
                autoFocus
                margin="dense"
                id="address"
                label="Address"
                type="text"
                fullWidth
                variant="filled"
                value={address}
                onChange={handleAddressChange}
              />
              <TextField
                autoFocus
                margin="dense"
                id="hobby"
                label="Hobby"
                type="text"
                fullWidth
                variant="filled"
                value={hobby}
                onChange={handleHobbyChange}
              />
            </DialogContent>
            <DialogActions>
              <Button variant="contained" onClick={handleCancel}>
                Cancel
              </Button>
              <Button variant="contained" onClick={handleSave}>
                Save
              </Button>
            </DialogActions>
          </Dialog>
        </div>
      </div>
      <div className="bodyContent">
        {userAdded ? (
          <div className="contentBoxUser">
            <div className="userBox">
              {userList.map((user, index) => (
                <div className="userData" key={index}>
                  <div className="userLeft">
                    <div>
                      <span className="label">Name: {user.name}</span>
                    </div>
                    <div>
                      <span className="label">Address: {user.address}</span>
                    </div>
                  </div>
                  <div className="userRight">
                    <span className="label">Hobby: {user.hobby}</span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ) : (
          <div className="contentBox">
            <span className="zero">0</span>
            <span className="User">User</span>
          </div>
        )}
        {showScrollButton && (
          <button className="scrollButton" onClick={scrollToTop}>
            Scroll to Top
          </button>
        )}
      </div>
    </div>
  );
};
